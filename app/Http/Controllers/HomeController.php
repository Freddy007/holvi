<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('index');
    }

    public function about()
    {
        return view('about');
    }

    public function services()
    {
        return view('services');
    }

    public function personal()
    {
        return view('personal.personal');
    }

    public function personalLoans()
    {
        return view('personal.loans');
    }

    public function personalPrestige()
    {
        return view('personal.prestige');
    }

    public function personalSavings()
    {
        return view('personal.savings');
    }

    public function commercial()
    {
        return view('commercial.commercial');
    }

    public function commercialTransactional()
    {
        return view('commercial.transactional_accounts');
    }

    public function commercialDigital()
    {
        return view('commercial.digital');
    }

    public function contactUs()
    {
        return view('contact_us');
    }

    public function getToken(): array
    {
        $token = \Str::random(60);

        return ['token' => $token, "hashed" => hash("sha256",$token)];
    }
}
