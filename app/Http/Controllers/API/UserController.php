<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    public function postUpdate(Request $request): \Illuminate\Http\JsonResponse
    {
        if (Auth::attempt(['email' => request('email'),
            'password' => request('password')])) {
            $token = \Str::random(60);

                User::whereEmail($request->email)->first()
                    ->forceFill([
                'api_token' => \hash('sha256', $token)
                    ])->save();

            return response()->json(["token" => $token], 200);
        }

        return response()->json(["Not Authorized"],401);
    }


    public function postDetails(Request $request): User
    {
      return $request->user();
    }
}
