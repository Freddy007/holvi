@extends("layouts.main-layout")

@section("content")

    <div class="header-area two">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="left">
                        <ul>
                            <li>
                                <i class='bx bx-location-plus'></i>
                                <a href="#">
{{--                                    43 Main Street Twickenham TW62 0TG, UK--}}
                                    ING House, Amstelveenseweg 522 1082 KL Amsterdam
                                </a>
                            </li>

                            <li>
                                <i class='bx bx-mail-send'></i>s
                                <a href="mailto:info@holvbank.com"><span class="__cf_email__" data-cfemail="452d2029292a05232c2b2a2b6b262a28">
                                        info@monezbank.com</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="right">
                        <ul>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-facebook'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-twitter'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-pinterest-alt'></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" target="_blank">
                                    <i class='bx bxl-linkedin'></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="index.html" class="logo">
                <img src="assets/img/logo_2.png" alt="Logo">
            </a>
        </div>
        @include("partials.nav")
    </div>

    <div class="banner-area-two">
        <div class="banner-slider-two owl-theme owl-carousel">
            <div class="banner-item banner-bg-two">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="banner-content">
                                <span>Bank your way</span>
                                <h1>Bank the way you want to</h1>
                                <p>Happy with the basics, or want the extra benefits your lifestyle demands? Bank Wherever you are. Whenever you want</p>
                                <div class="banner-btn-area">
                                    <a class="common-btn two" href="{{route("contact-us")}}">
                                        Contact Us
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="banner-item banner-bg-three">
                <div class="d-table">
                    <div class="d-table-cell">
                        <div class="container">
                            <div class="banner-content">
                                <span>No Obstacles</span>
                                <h1>Business without borders</h1>
                                <p>Bespoke Finacial Solutions carefully tailored and thought through to meet your business needs. </p>
                                <div class="banner-btn-area">
                                    <a class="common-btn two" href="{{route("contact-us")}}">
                                        Contact Us
                                        <span></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>


    <div class="about-area-two pt-100 pb-70">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="about-content">
                        <div class="section-title two">
                            <span class="sub-title">About Us</span>
                            <h2>We Help Our Clients To Achieve Their Desired Goals</h2>
                        </div>
                        <p class="about-p">Over the past two decades, the banking and financial services industry has seen significant improvements in the quality of
                            service delivery and the provision of superior financial products
                            and packages and We have earned a well-deserved reputation as a customer oriented, business friendly and socially relevant bank.</p>
                        <ul>
                            <li>
                                <i class="flaticon-bar-chart"></i>
                                <h3>Sponsorships</h3>
                                <p>We’re committed to developing platforms to engage with and support communities and businesses wherever we
                                    operate, building our brand through relevant and meaningful sponsorship. </p>
                            </li>
                            <li>
                                <i class="flaticon-consulting"></i>
                                <h3>Careers</h3>
                                <p>
                                    There’s never been a more exciting time to be a part of our story. Move your career forward with us, as we continue to make great things happen.
                                </p>
                            </li>
                        </ul>
                        <a class="common-btn two" href="#">
                            Read More
                            <span></span>
                        </a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="about-img">
                        <img src="assets/img/about/about4.jpg" alt="About">
                        <img src="assets/img/about/about-shape6.png" alt="Shape">
                        <img src="assets/img/about/about-shape7.png" alt="Shape">
                        <img src="assets/img/about/about-shape8.png" alt="Shape">
                    </div>
                </div>
            </div>
        </div>
    </div>


    <section class="projects-area pt-100 pb-70">
        <div class="container">
            <div class="section-title two">
                <span class="sub-title">Latest Projects</span>
                <h2>We Have Completed Latest Projects</h2>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item">
                        <img src="assets/img/projects/projects1.jpg" alt="Projects">
                        <div class="inner">
                            <h3>
                                <a href="project-details.html">Investment Trading</a>
                            .</h3>
                            <a class="projects-btn" href="#">Trading</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item">
                        <img src="assets/img/projects/projects2.jpg" alt="Projects">
                        <div class="inner">
                            <h3>
                                <a href="project-details.html">Financial Growth</a>
                            </h3>
                            <a class="projects-btn" href="#">Finance</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item">
                        <img src="assets/img/projects/projects3.jpg" alt="Projects">
                        <div class="inner">
                            <h3>
                                <a href="project-details.html">Fund Management</a>
                            </h3>
                            <a class="projects-btn" href="#">Management</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item">
                        <img src="assets/img/projects/projects4.jpg" alt="Projects">
                        <div class="inner">
                            <h3>
                                <a href="#">Online Payment</a>
                            </h3>
                            <a class="projects-btn" href="#">Payment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="services-area-two pt-100 pb-70">
        <div class="container">
            <div class="section-title two">
                <span class="sub-title">Services</span>
                <h2>Exceptional Finon Services</h2>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item-two">
                        <div class="top">
                            <img src="assets/img/services/services1.jpg" alt="Services">
                        </div>
                        <div class="bottom">
                            <h3>
                                <a href="service-details.html">Investment Planning</a>
                            </h3>
                            <p>
                                We help in identifying financial goals and thorough building plan.
                            </p>
                            <div class="services-btn">
                                <i class='bx bx-right-arrow-alt'></i>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item-two">
                        <div class="top">
                            <img src="assets/img/services/services2.jpg" alt="Services">
                        </div>
                        <div class="bottom">
                            <h3>
                                <a href="service-details.html">Financial Consultancy</a>
                            </h3>
                            <p>We have experts to help you construct personalized financial plans and goals</p>
                            <div class="services-btn">
                                <i class='bx bx-right-arrow-alt'></i>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item-two">
                        <div class="top">
                            <img src="assets/img/services/services3.jpg" alt="Services">
                        </div>
                        <div class="bottom">
                            <h3>
                                <a href="#">Online Banking</a>
                            </h3>
                            <p>We have a secured platform for online transactions. </p>
                            <div class="services-btn">
                                <i class='bx bx-right-arrow-alt'></i>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-lg-4">
                    <div class="services-item-two">
                        <div class="top">
                            <img src="assets/img/services/services4.jpg" alt="Services">
                        </div>
                        <div class="bottom">
                            <h3>
                                <a href="#">Loans</a>
                            </h3>
                            <p>
                                Our borrowing facilities are designed to bring you a high level
                                of satisfaction.
                            </p>
                            <div class="services-btn">
                                <i class='bx bx-right-arrow-alt'></i>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item-two">
                        <div class="top">
                            <img src="assets/img/services/services5.jpg" alt="Services">
                        </div>
                        <div class="bottom">
                            <h3>
                                <a href="service-details.html">Prestige Banking</a>
                            </h3>
                            <p>
                                The Prestige banking experience was created for those who believe in uncompromising excellence.
                            </p>
                            <div class="services-btn">
                                <i class='bx bx-right-arrow-alt'></i>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item-two">
                        <div class="top">
                            <img src="assets/img/services/services6.jpg" alt="Services">
                        </div>
                        <div class="bottom">
                            <h3>
                                <a href="service-details.html">Savings & Investments</a>
                            </h3>
                            <p>
                                An account tailor-made for the future and according to your investment capacity.
                            </p>
                            <div class="services-btn">
                                <i class='bx bx-right-arrow-alt'></i>
                                <a href="#">Read More</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="projects-area-two pt-100 pb-70">
        <div class="container">
            <div class="section-title two">
{{--                <span class="sub-title">Projects</span>--}}
                <h2>What You Get</h2>
            </div>
            <div class="row">
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item-two">
                        <img src="assets/img/projects/project-shape1.png" alt="Shape">
                        <img src="assets/img/projects/project-shape2.png" alt="Shape">
                        <i class="flaticon-goal icon"></i>
                        <h3>
                            <a href="#">Competitive rates</a>
                        </h3>
                        <p>Get competitive rates offering guaranteed returns.</p>
{{--                        <a class="projects-btn" href="project-details.html">--}}
{{--                            Read More--}}
{{--                            <i class='bx bx-right-arrow-alt'></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item-two">
                        <img src="assets/img/projects/project-shape1.png" alt="Shape">
                        <img src="assets/img/projects/project-shape2.png" alt="Shape">
                        <i class="flaticon-support icon"></i>
                        <h3>
                            <a href="#">Control</a>
                        </h3>
                        <p>Manage your overdraft limits from your device using our mobile banking app.</p>
{{--                        <a class="projects-btn" href="#">--}}
{{--                            Read More--}}
{{--                            <i class='bx bx-right-arrow-alt'></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item-two">
                        <img src="assets/img/projects/project-shape1.png" alt="Shape">
                        <img src="assets/img/projects/project-shape2.png" alt="Shape">
                        <i class="flaticon-investment-insurance icon"></i>
                        <h3>
                            <a href="project-details.html">Independence</a>
                        </h3>
                        <p>Bank the way you want to, whenever you want to – online, in-app or at our ATMs.</p>
{{--                        <a class="projects-btn" href="project-details.html">--}}
{{--                            Read More--}}
{{--                            <i class='bx bx-right-arrow-alt'></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3">
                    <div class="projects-item-two">
                        <img src="assets/img/projects/project-shape1.png" alt="Shape">
                        <img src="assets/img/projects/project-shape2.png" alt="Shape">
                        <i class="flaticon-refinancing icon"></i>
                        <h3>
                            <a href="project-details.html">Security</a>
                        </h3>
                        <p>Know your money’s safe when making payments in-store, online or overseas.</p>
{{--                        <a class="projects-btn" href="project-details.html">--}}
{{--                            Read More--}}
{{--                            <i class='bx bx-right-arrow-alt'></i>--}}
{{--                        </a>--}}
                    </div>
                </div>
            </div>
        </div>
    </section>


    <div class="counter-area ptb-100">
        <div class="container">
            <div class="counter-wrap">
                <div class="counter-shape">
                    <img src="assets/img/counter-shape1.png" alt="Shape">
                    <img src="assets/img/counter-shape2.png" alt="Shape">
                </div>
                <div class="row">
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="counter-item">
                            <i class="flaticon-project-management"></i>
                            <h3>
                                <span class="odometer" data-count="3569">00</span>
                            </h3>
                            <p>Projects Completed</p>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="counter-item">
                            <i class="flaticon-like"></i>
                            <h3>
                                <span class="odometer" data-count="4269">00</span>
                            </h3>
                            <p>Satisfied Clients</p>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="counter-item">
                            <i class="flaticon-trophy"></i>
                            <h3>
                                <span class="odometer" data-count="6421">00</span>
                            </h3>
                            <p>International Awards</p>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-lg-3">
                        <div class="counter-item right-border">
                            <i class="flaticon-team"></i>
                            <h3>
                                <span class="odometer" data-count="3351">00</span>
                            </h3>
                            <p>Team Members</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="video-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="video-item">
                    <div class="video-wrap">
                        <a href="#" class="popup-youtube">
                            <i class='bx bx-play'></i>
                        </a>
                    </div>
                    <span>Watch Our Latest Video For Better Innovation</span>
                </div>
            </div>
        </div>
    </div>


    <div class="works-area-two ptb-100">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="works-img">
                        <img src="assets/img/works-shape1.png" alt="Shape">
                        <img src="assets/img/works-main.png" alt="Works">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="works-content">
                        <div class="section-title two">
                            <span class="sub-title">Opening an account</span>
                            <h2>Open an Account</h2>
                            <p>
                               You must meet the basic requirements of opening an account.

                            </p>
                        </div>
                        <ul>
                            <li>
                                <i class='bx bx-check'></i>
                                <h3>Register with Personal Details</h3>
                                <p>
                                   Fill in form with your valid registration details as it is required to open account with us.
                                </p>
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                <h3>Accept Terms and Conditions</h3>
                                <p>
                                    Carefully read through the terms and conditions in relation to account
                                    holders and accept them in order to proceed to the next stage.
                                </p>
                            </li>
                            <li>
                                <i class='bx bx-check'></i>
                                <h3>Account Created in 24 hours</h3>
                                <p>
                                    After step 1, and step 2, the account is successfully created and account details sent to your registered email.
                                </p>
                            </li>
                        </ul>
                        <a class="common-btn two" href="https://holvbank.herokuapp.com" target="_blank">
                            Learn More
                            <span></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="book-area pb-100">
        <div class="container">
            <div class="book-content">
                <div class="book-shape">
                    <img src="assets/img/book-shape3.png" alt="Shape">
                    <img src="assets/img/book-shape4.png" alt="Shape">
                </div>
                <h2>Are You Ready? Book Appointment Now</h2>
{{--                <p>Get your Quote or Call <a href="tel: +447405239482"> +44 7405 239482</a></p>--}}
                <p>Get your Quote or Call <a href="tel: +319 700 50 33 066"> +319 700 50 33 066</a></p>
                <a class="common-btn" href="{{route("contact-us")}}">
                    Read More
                    <span></span>
                </a>
            </div>
        </div>
    </div>


    <section class="testimonials-area-two ptb-100">
        <div class="container">
            <div class="section-title two">
                <span class="sub-title">Testimonials</span>
                <h2>What Our Clients Says</h2>
            </div>
            <div class="testimonials-slider owl-theme owl-carousel">
                <div class="testimonials-item">
                    <h2>
                        We have had a strong relationship with Monezbank since its founding in 2004. Their personalized
                        service and expertise got us through COVID-19 by securing an SBA PPP loan and funds to
                        protect our business and retain our excellent staff.
                    </h2>
                    <img src="" alt="Testimonials">
                    <h3>Pat McQuillan</h3>
                    <span>Owner</span>
                    <i class='bx bxs-quote-alt-left bx-flip-horizontal'></i>
                </div>
                <div class="testimonials-item">
                    <h2>
                        Holv Bank has supported my business since day one. When I was just starting out, they took the time to
                        understand my business and came up with creative ways to support funding. We're now a multi-million-dollar business.
                        It has been a great relationship for the past 11 years
                    </h2>
                    <img src="" alt="Testimonials">
                    <h3>Chris Zephro</h3>
                    <span>Owner</span>
                    <i class='bx bxs-quote-alt-left bx-flip-horizontal'></i>
                </div>
{{--                <div class="testimonials-item">--}}
{{--                    <h2>--}}
{{--                        Without the support and expertise of our local bank, we could not have grown as quickly as desired.--}}
{{--                        Our local bank knows our business and always responds to meet our needs--}}
{{--                    </h2>--}}
{{--                    <img src="" alt="Testimonials">--}}
{{--                    <h3>Eric & Ellen Gil</h3>--}}
{{--                    <span>Owners</span>--}}
{{--                    <i class='bx bxs-quote-alt-left bx-flip-horizontal'></i>--}}
{{--                </div>--}}
            </div>
        </div>
    </section>


{{--    <section class="blog-area two pt-100 pb-70">--}}
{{--        <div class="container">--}}
{{--            <div class="section-title two">--}}
{{--                <span class="sub-title">Latest News</span>--}}
{{--                <h2>Latest News From Blog</h2>--}}
{{--            </div>--}}
{{--            <div class="row">--}}
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="blog-item">--}}
{{--                        <div class="top">--}}
{{--                            <img src="assets/img/blog/blog1.jpg" alt="Blog">--}}
{{--                        </div>--}}
{{--                        <div class="bottom">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <i class='bx bx-user'></i>--}}
{{--                                    by--}}
{{--                                    <a href="#">Admin</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <i class='bx bx-calendar'></i>--}}
{{--                                    27 November, 2020--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h3>--}}
{{--                                <a href="blog-details.html">Financial planning is the best invest for corporate business</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo</p>--}}
{{--                            <a class="blog-btn" href="blog-details.html">Read More</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 col-lg-4">--}}
{{--                    <div class="blog-item">--}}
{{--                        <div class="top">--}}
{{--                            <img src="assets/img/blog/blog2.jpg" alt="Blog">--}}
{{--                        </div>--}}
{{--                        <div class="bottom">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <i class='bx bx-user'></i>--}}
{{--                                    by--}}
{{--                                    <a href="#">Admin</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <i class='bx bx-calendar'></i>--}}
{{--                                    28 November, 2020--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h3>--}}
{{--                                <a href="blog-details.html">Largest demand partnership building with agency</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo</p>--}}
{{--                            <a class="blog-btn" href="blog-details.html">Read More</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-sm-6 offset-sm-3 offset-lg-0 col-lg-4">--}}
{{--                    <div class="blog-item">--}}
{{--                        <div class="top">--}}
{{--                            <img src="assets/img/blog/blog3.jpg" alt="Blog">--}}
{{--                        </div>--}}
{{--                        <div class="bottom">--}}
{{--                            <ul>--}}
{{--                                <li>--}}
{{--                                    <i class='bx bx-user'></i>--}}
{{--                                    by--}}
{{--                                    <a href="#">Admin</a>--}}
{{--                                </li>--}}
{{--                                <li>--}}
{{--                                    <i class='bx bx-calendar'></i>--}}
{{--                                    29 November, 2020--}}
{{--                                </li>--}}
{{--                            </ul>--}}
{{--                            <h3>--}}
{{--                                <a href="blog-details.html">How to become top conference for business growth</a>--}}
{{--                            </h3>--}}
{{--                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo</p>--}}
{{--                            <a class="blog-btn" href="blog-details.html">Read More</a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </section>--}}


    <div class="logo-area ptb-100">
        <div class="container">
            <div class="logo-slider owl-theme owl-carousel">
                <div class="logo-item">
                    <img src="assets/img/logo/logo1.png" alt="Logo">
                </div>
                <div class="logo-item">
                    <img src="assets/img/logo/logo2.png" alt="Logo">
                </div>
                <div class="logo-item">
                    <img src="assets/img/logo/logo3.png" alt="Logo">
                </div>
                <div class="logo-item">
                    <img src="assets/img/logo/logo4.png" alt="Logo">
                </div>
                <div class="logo-item">
                    <img src="assets/img/logo/logo5.png" alt="Logo">
                </div>
            </div>
        </div>
    </div>


@endsection
