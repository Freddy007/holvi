@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="/" class="logo">
                {{--                <img src="assets/img/logo-two.png" alt="Logo">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="/">
                        {{--                        <img src="assets/img/logo.png" alt="Logo">--}}
                        <img src="assets/img/logo_2.png" alt="Logo">

                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")

                        @include("partials.side_nav")

                    </div>
                </nav>
            </div>
        </div>
    </div>


    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>About Us</h2>
                        <ul>
                            <li>
                                <a href="{{route("home")}}">Home</a>
                            </li>
                            <li>
                                <span>About Us</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="service-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="details-item">
                        <div class="details-img">
                            <img src="assets/img/services/service-details1.jpg" alt="Details">
                            <h2>About Us</h2>
                            <p>
                                Monezbank set out to become a world-class financial institution that provides superior returns for all stakeholders.
                                People who are professional and proactive, state of the art technology, exceptional Corporate Governance standards,
                                good knowledge of the Local Market, and above all, a Customer-Centric Culture.

                                Fidelity commenced business in October 1998 as a Discount House. Monezbank Discount House attracted a rich
                                client base and was noted for its innovative and attractive investment product offerings, making her
                                the discount house of choice. With the quality of services offered, our customers requested for a deeper and
                                richer business relationship, making it logical to move into the banking sector.
                                On the 28th of June 2006, we obtained a universal banking license.
                            </p>

                            <p>
                                Within six months of opening our doors for business, we had already broken even, a remarkable feat that set the stage for
                                our expansion which can be found not only in our steadily growing network of branches, mobile banking platform and in our solid base
                                of satisfied customers, but in the various awards we have garnered in the industry for customer service, digital product offerings,
                                trade deals, human resource,
                                corporate social responsibility, and overall best bank. We have won more than 60 awards for excellence since our inception.
                            </p>
                        </div>
                    </div>
                </div>


                @include("partials.services_list")

            </div>
        </div>
    </div>

@endsection
