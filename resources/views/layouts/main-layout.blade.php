<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="UTF-8">
    <meta name="viewport"  content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/meanmenu.css">
    <link rel="stylesheet" href="assets/css/boxicons.min.css">
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="assets/css/animate.min.css">
    <link rel="stylesheet" href="assets/fonts/flaticon.css">
    <link rel="stylesheet" href="assets/css/odometer.min.css">
    <link rel="stylesheet" href="assets/css/nice-select.min.css">
    <link rel="stylesheet" href="assets/css/magnific-popup.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">
    <title>Monezbank - Finance and Investment </title>
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

</head>

<body>

<div class="loader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="spinner">
                <div class="double-bounce1"></div>
                <div class="double-bounce2"></div>
            </div>
        </div>
    </div>
</div>

@yield("content")

<footer class="footer-area two pt-100 pb-70">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-4">
                <div class="footer-item">
                    <div class="footer-logo">
                        <a class="logo" href="/">
{{--                            <img src="assets/img/logo-four.png" alt="Logo">--}}
                            <img src="assets/img/logo_2.png" alt="Logo">
                        </a>
                        <p>
                            We’re more than just a bank. We look beyond the financial outcome to create more value socially, economically and environmentally.
                        </p>
                        <ul>
                            <li>
                                <i class='bx bx-phone-call'></i>
                                <span>Phone:</span>
                                <a href="tel:+447405239482 f">+44 7405 239482 </a>
                            </li>
                            <li>
                                <i class='bx bx-mail-send'></i>
                                <span>Email:</span>
                                <a href=""><span class="__cf_email__" data-cfemail="86eee3eaeae9c6e0efe8e9e8a8e5e9eb">info@monezbankbank.com</span></a>
                            </li>
                            <li>
                                <i class='bx bx-current-location'></i>
                                <span>Address:</span>
                                <a href="#">43 Main Street Twickenham TW62 0TG, UK</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-2">
                <div class="footer-item">
                    <div class="footer-links">
                        <h3>Quick Links</h3>
                        <ul>
                            <li>
                                <a href="">About</a>
                            </li>
                            <li>
                                <a href="{{route("personal.loans")}}">Loans</a>
                            </li>
                            <li>
                                <a href="{{route("personal.prestige")}}">Prestige Banking</a>
                            </li>
                            <li>
                                <a href="{{route("personal.savings")}}">Savings & Investments</a>
                            </li>
                            <li>
                                <a href="#">Privacy Policy</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="footer-item">
                    <h3>Newsletter</h3>
                    <div class="footer-newsletter">
                        <p>Subscribe to our newsletter.</p>
                        <form class="newsletter-form" data-toggle="validator">
                            <input type="email" class="form-control" placeholder="Enter email address" name="EMAIL" required autocomplete="off">
                            <button class="btn common-btn" type="submit">
                                Subscribe
                                <span></span>
                            </button>
                            <div id="validator-newsletter" class="form-result"></div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="footer-item">
                    <div class="footer-links">
                        <h3>What We Do</h3>
                        <ul>
                            <li>
                                <a href="{{route("commercial.transactional")}}">Transactional Accounts</a>
                            </li>
                            <li>
                                <a href="{{route("commercial.digital")}}">Digital Banking</a>
                            </li>
                            <li>
                                <a href="#">Investment Trending</a>
                            </li>
                            <li>
                                <a href="#">Wealth Commitment</a>
                            </li>
                            <li>
                                <a href="#">Our Services</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="copyright-area two">
    <div class="container">
        <div class="copyright-item">
{{--            <p>Copyright @2020 Design & Developed by <a href="https://envytheme.com/" target="_blank">EnvyTheme</a></p>--}}
        </div>
    </div>
</div>

<div class="go-top">
    <i class='bx bxs-up-arrow'></i>
    <i class='bx bxs-up-arrow'></i>
</div>

<script data-cfasync="false"
        src="../../cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="assets/js/jquery-3.5.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>

<script src="assets/js/form-validator.min.js"></script>

<script src="assets/js/contact-form-script.js"></script>

<script src="assets/js/jquery.ajaxchimp.min.js"></script>

<script src="assets/js/jquery.meanmenu.js"></script>

<script src="assets/js/owl.carousel.min.js"></script>

<script src="assets/js/wow.min.js"></script>

<script src="assets/js/odometer.min.js"></script>
<script src="assets/js/jquery.appear.min.js"></script>

<script src="assets/js/jquery.nice-select.min.js"></script>

<script src="assets/js/jquery.magnific-popup.min.js"></script>

<script src="assets/js/custom.js"></script>
</body>

</html>
