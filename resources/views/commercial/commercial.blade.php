@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a class="logo" href="/">
{{--                <img alt="Logo" src="assets/img/logo-two.png">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="/">
{{--                        <img alt="Logo" src="assets/img/logo.png">--}}
                        <img src="assets/img/logo_2.png" alt="Logo">

                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")

                        <div class="side-nav">
                            <div class="dropdown nav-flag-dropdown">
                                <button aria-expanded="false" aria-haspopup="true" class="btn dropdown-toggle" data-toggle="dropdown" id="dropdownMenuButton" type="button">
                                    <img alt="Flag" src="assets/img/flag1.jpg">
                                    Eng
                                    <i class='bx bx-chevron-down'></i>
                                </button>
                                <div aria-labelledby="dropdownMenuButton" class="dropdown-menu">
                                    <a class="dropdown-item" href="#">
                                        <img alt="Flag" src="assets/img/flag2.jpg">
                                        Ger
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <img alt="Flag" src="assets/img/flag3.jpg">
                                        Isr
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <img alt="Flag" src="assets/img/flag4.jpg">
                                        USA
                                    </a>
                                </div>
                            </div>
                            <a class="consultant-btn" href="#">
                                Free Consultant
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>

    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>Services Style One</h2>
                        <ul>
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <span>Services Style One</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

{{--    <div class="header-area two">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="left">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <i class='bx bx-location-plus'></i>--}}
{{--                                <a href="#">--}}
{{--                                    504 White St . Dawsonville, New York--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bx-mail-send'></i>--}}
{{--                                <a href="https://templates.envytheme.com/cdn-cgi/l/email-protection#6c04090000032c0a05020302420f0301"><span class="__cf_email__" data-cfemail="452d2029292a05232c2b2a2b6b262a28">[email&#160;protected]</span></a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="right">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-facebook'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-twitter'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-pinterest-alt'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-linkedin'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--    <div class="navbar-area sticky-top">--}}

{{--        <div class="mobile-nav">--}}
{{--            <a href="index.html" class="logo">--}}
{{--                <img src="assets/img/logo-four.png" alt="Logo">--}}
{{--            </a>--}}
{{--        </div>--}}
{{--        @include("partials.nav")--}}
{{--    </div>--}}
    <section class="services-area four ptb-100">
        <div class="services-shape">
            <img alt="Shape" src="assets/img/services/services-shape1.png">
            <img alt="Shape" src="assets/img/services/services-shape2.png">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item">
                        <i class="flaticon-deposit icon"></i>
                        <h3>
                            <a href="service-details.html">Invest Planning</a>
                        </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna</p>
                        <div class="services-btn">
                            <i class='bx bx-right-arrow-alt'></i>
                            <a href="service-details.html">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item">
                        <i class="flaticon-loan icon"></i>
                        <h3>
                            <a href="service-details.html">Financial Consultancy</a>
                        </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna</p>
                        <div class="services-btn">
                            <i class='bx bx-right-arrow-alt'></i>
                            <a href="service-details.html">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item">
                        <i class="flaticon-online-banking icon"></i>
                        <h3>
                            <a href="service-details.html">Online Banking & Loans</a>
                        </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna</p>
                        <div class="services-btn">
                            <i class='bx bx-right-arrow-alt'></i>
                            <a href="service-details.html">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item">
                        <i class="flaticon-education-cost icon"></i>
                        <h3>
                            <a href="service-details.html">Education Loans</a>
                        </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna</p>
                        <div class="services-btn">
                            <i class='bx bx-right-arrow-alt'></i>
                            <a href="service-details.html">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item">
                        <i class="flaticon-loan-1 icon"></i>
                        <h3>
                            <a href="service-details.html">House Loans</a>
                        </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna</p>
                        <div class="services-btn">
                            <i class='bx bx-right-arrow-alt'></i>
                            <a href="service-details.html">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-4">
                    <div class="services-item">
                        <i class="flaticon-consulting icon"></i>
                        <h3>
                            <a href="service-details.html">Consultancy</a>
                        </h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore dolore magna</p>
                        <div class="services-btn">
                            <i class='bx bx-right-arrow-alt'></i>
                            <a href="service-details.html">
                                Read More
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pagination-area">
                <ul>
                    <li>
                        <a href="#">
                            <i class='bx bx-chevron-left'></i>
                        </a>
                    </li>
                    <li>
                        <a class="active" href="#">
                            1
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            2
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            3
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            <i class='bx bx-chevron-right'></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </section>
@endsection
