@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="index.html" class="logo">
                {{--                <img src="assets/img/logo-two.png" alt="Logo">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="/">
                        {{--                        <img src="assets/img/logo.png" alt="Logo">--}}
                        <img src="assets/img/logo_2.png" alt="Logo">

                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")

                        @include("partials.side_nav")

                    </div>
                </nav>
            </div>
        </div>
    </div>


    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>Digital Banking</h2>
                        <ul>
                            <li>
                                <a href="{{route("home")}}">Home</a>
                            </li>
                            <li>
                                <span>Digital Banking</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


{{--    <div class="header-area two">--}}
{{--        <div class="container">--}}
{{--            <div class="row align-items-center">--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="left">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <i class='bx bx-location-plus'></i>--}}
{{--                                <a href="#">--}}
{{--                                    504 White St . Dawsonville, New York--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <i class='bx bx-mail-send'></i>--}}
{{--                                <a href="https://templates.envytheme.com/cdn-cgi/l/email-protection#6c04090000032c0a05020302420f0301"><span class="__cf_email__" data-cfemail="452d2029292a05232c2b2a2b6b262a28">[email&#160;protected]</span></a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-lg-6">--}}
{{--                    <div class="right">--}}
{{--                        <ul>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-facebook'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-twitter'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-pinterest-alt'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                            <li>--}}
{{--                                <a href="#" target="_blank">--}}
{{--                                    <i class='bx bxl-linkedin'></i>--}}
{{--                                </a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}


{{--    <div class="navbar-area sticky-top">--}}


    <div class="service-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="details-item">
                        <div class="details-img">
                            <img src="assets/img/services/service-details1.jpg" alt="Details">
                            <h2>Digital Banking</h2>
                            <p>
                                Freedom to manage your money according to your transactional needs.
                                This account provides unrestricted access to clients’ money anytime and anywhere via our branches or electronic channels.
                            </p>
                        </div>
                        <div class="details-business">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details2.jpg" alt="Details">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details3.jpg" alt="Details">
                                    </div>
                                </div>
                            </div>

                            <h3>PAYMENTS (MONEZBANK ONLINE)</h3>
                            <p>
                                Monezbank Online (web, mobile) provides an end-to-end electronic platform with single access for transaction initiation and reporting.
                                Monezbank Online Access provides an alternate channel to integrate into clients’ in-house system (e.g. SAP, SAGE, ORACLE, and SUN).
                                This offers clients the ease of banking directly from their in-house systems.
                            </p>

                            <h3>PAYMENTS COLLECTIONS</h3>
                            <p>
                                Integrated into your website to enable online payments using debit/credit cards (VISA and MasterCard).
                                Integration to your in-house billing system for real-time updates on all receivables.
                            </p>

                        </div>
                    </div>
                </div>

                @include("partials.services_list")

            </div>
        </div>
    </div>

@endsection
