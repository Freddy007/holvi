@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="/" class="logo">
{{--                <img src="assets/img/logo-two.png" alt="Logo">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="index.html">
                        <img src="assets/img/logo.png" alt="Logo">
                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")

                        <div class="side-nav">
                            <div class="dropdown nav-flag-dropdown">
                                <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <img src="assets/img/flag1.jpg" alt="Flag">
                                    Eng
                                    <i class='bx bx-chevron-down'></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">
                                        <img src="assets/img/flag2.jpg" alt="Flag">
                                        Ger
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <img src="assets/img/flag3.jpg" alt="Flag">
                                        Isr
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <img src="assets/img/flag4.jpg" alt="Flag">
                                        USA
                                    </a>
                                </div>
                            </div>
                            <a class="consultant-btn" href="#">
                                Internet Banking
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>


    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>Personal Loans</h2>
                        <ul>
                            <li>
                                <a href="{{route("home")}}">Home</a>
                            </li>
                            <li>
                                <span>Personal Loans</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="service-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="details-item">
                        <div class="details-img">
                            <img src="assets/img/services/service-details1.jpg" alt="Details">
                            <h2>Personal Loans</h2>
                            <p>
                                Why restrict your diet when you can enjoy a feast? Our borrowing facilities are
                                designed to bring you a high level of satisfaction with your banking transactions and interactions.
                            </p>
{{--                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>--}}
                        </div>
                        <div class="details-business">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details2.jpg" alt="Details">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details3.jpg" alt="Details">
                                    </div>
                                </div>
                            </div>
                            <h3>Salary Backed Loans</h3>
                            <p>
                                Our Salary Backed Loan is designed to support employees of profiled (selected) organizations to meet their short to medium term financial needs.
                                It offers a simple loan process, flexible repayment plan and competitive loan rates.
                            </p>
{{--                            <ul>--}}
{{--                                <li>Complete Guide To Mechanical</li>--}}
{{--                                <li>Business & Consulting Agency</li>--}}
{{--                                <li>Award Wining Business</li>--}}
{{--                            </ul>--}}
{{--                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin. Mliterature from 45 BC, making it over 2000 years old. Lorealintock The Extremes of Good and dummy Eviyr. The standard chunk of Lorem Ipsum used sfertyui ince the 1500s is reproduced below for those interested. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dom, totam rem ape Donec ornare unde omnis iste natus.</p>--}}
{{--                        </div>--}}

                        <h3>Employer Managed Personal Loan</h3>
                        <p>
                            The Employer Managed Personal Loan is a medium-term facility designed for salaried employees working in organizations listed under our approved Employer Managed Salaried Programme.
                            It offers enhanced benefits, repayment plan and lower rates.
                        </p>
{{--                    </div>--}}


                    <h3>Auto Loan</h3>
                    <p>
                        Get your dream car with our Personal Auto Loan facility tailor-made for you. This is a medium-term facility available to salaried employees of organizations under Fidelity Bank’s Employer Managed Relationship.
                        Simply apply for a Personal Auto Loan and enjoy a flexible repayment plan at competitive rates.
                    </p>
{{--                </div>--}}

                <h3>Fast and Easy Loan</h3>
                <p>
                    Why use your future savings or investment now when you can access a Fast and Easy loan,
                    and pay back over 12 months period at the lowest net interest rate. Monezbank Fast and Easy loan is a short-term facility designed to give existing and new bank customers access to easy
                    and quick loans in less than 24 hrs at a low REAL interest rate of 10% p.a.
                </p>
            </div>

{{--                    <div class="details-faq">--}}
{{--                            <div class="section-title">--}}
{{--                                <span class="sub-title">Finon FAQ</span>--}}
{{--                                <h2>Frequently Asked Questions</h2>--}}
{{--                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                            </div>--}}
{{--                            <div class="faq-item">--}}
{{--                                <ul class="accordion">--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What kind of financial consultancy you need--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            How we help you for your business--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What is Firewall and why it is used?--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What steps will you take to remove risk of finance--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget-area">
                        <div class="services widget-item">
                            <h3>Services List</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        Cash Investment
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Personal Insurance
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Education Loan
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Financial Planning
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="download widget-item">
                            <h3>Download</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class='bx bxs-file-pdf'></i>
                                        Presentation pdf
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class='bx bx-notepad'></i>
                                        Wordfile.doc
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="contact widget-item">
                            <h3>Contact</h3>
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your email">
                                </div>
                                <div class="form-group">
                                    <textarea id="your-message" rows="8" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <button type="submit" class="btn common-btn">
                                    Send Message
                                    <span></span>
                                </button>
                            </form>
                        </div>
                        <div class="consultation">
                            <img src="assets/img/services/service-details4.jpg" alt="Details">
                            <div class="inner">
                                <h3>Need Any Consultation</h3>
                                <a class="common-btn" href="contact.html">
                                    Send Message
                                    <span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
