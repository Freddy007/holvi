@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="index.html" class="logo">
{{--                <img src="assets/img/logo-two.png" alt="Logo">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="/">
{{--                        <img src="assets/img/logo.png" alt="Logo">--}}
                        <img src="assets/img/logo_2.png" alt="Logo">

                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")

                        @include("partials.side_nav")

                    </div>
                </nav>
            </div>
        </div>
    </div>


    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>Savings & Investments</h2>
                        <ul>
                            <li>
                                <a href="{{route("home")}}">Home</a>
                            </li>
                            <li>
                                <span>Savings and Investments</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="service-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="details-item">
                        <div class="details-img">
                            <img src="assets/img/services/service-details1.jpg" alt="Details">
                            <h2>Savings and Investments</h2>
                            <p>An account tailor-made for the future and for rainy days according to your investment capacity.
                                These accounts provide high, above-market rates and are a good option for long-term strategic growth.
                            </p>
{{--                            <p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.</p>--}}
                        </div>
                        <div class="details-business">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details2.jpg" alt="Details">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details3.jpg" alt="Details">
                                    </div>
                                </div>
                            </div>

                            <h3>Holvbank Reserve Account</h3>
                            <p>
                                Whatever you are saving for, the Holvbank Reserve account makes
                                it easier to achieve your target with ease and convenience.
                            </p>

                            <h3>Fixed Term Deposit</h3>
                            <p>
                                Our Fixed Deposit investment guarantees the security of your funds at high-interest rates than savings accounts.
                                It offers you the flexibility to choose your preferred term of investment and an option to roll-over automatically.
                            </p>

                            <h3>Flip USD Account</h3>
                            <p>
                                The FLIP USD account is a US dollar denominated short term investment which guarantees competitive rate on your dollar funds.
                                Enjoy the flexibility to build-up USD to meet future plans by making monthly deposits into your FLIP USD.
                            </p>

                            <h3>Smart Account</h3>
                            <p>
                                No matter how low your income is, secure and manage your money the smart way with the Smart Account.
                                Get an instant bank account and ATM debit card with any valid national ID.
                            </p>

                            <h3>Easy Save Account</h3>
                            <p>
                                Save as you spend with the Fidelity Easy Save Account (FESA). Enjoy automatic savings from your t
                                ransaction account to your FESA whenever you make withdrawal, payment or transfer on your account.
                            </p>
                        </div>
{{--                        <div class="details-faq">--}}
{{--                            <div class="section-title">--}}
{{--                                <span class="sub-title">Finon FAQ</span>--}}
{{--                                <h2>Frequently Asked Questions</h2>--}}
{{--                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                            </div>--}}
{{--                            <div class="faq-item">--}}
{{--                                <ul class="accordion">--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What kind of financial consultancy you need--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            How we help you for your business--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What is Firewall and why it is used?--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What steps will you take to remove risk of finance--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget-area">
                        <div class="services widget-item">
                            <h3>Services List</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        Cash Investment
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Personal Insurance
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Education Loan
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Financial Planning
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="download widget-item">
                            <h3>Download</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class='bx bxs-file-pdf'></i>
                                        Presentation pdf
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class='bx bx-notepad'></i>
                                        Wordfile.doc
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="contact widget-item">
                            <h3>Contact</h3>
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your email">
                                </div>
                                <div class="form-group">
                                    <textarea id="your-message" rows="8" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <button type="submit" class="btn common-btn">
                                    Send Message
                                    <span></span>
                                </button>
                            </form>
                        </div>
                        <div class="consultation">
                            <img src="assets/img/services/service-details4.jpg" alt="Details">
                            <div class="inner">
                                <h3>Need Any Consultation</h3>
                                <a class="common-btn" href="contact.html">
                                    Send Message
                                    <span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
