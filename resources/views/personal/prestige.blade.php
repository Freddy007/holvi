@extends("layouts.main-layout")

@section("content")

    <div class="navbar-area sticky-top">

        <div class="mobile-nav">
            <a href="index.html" class="logo">
{{--                <img src="assets/img/logo-two.png" alt="Logo">--}}
                <img src="assets/img/logo_2.png" alt="Logo">

            </a>
        </div>

        <div class="main-nav">
            <div class="container">
                <nav class="navbar navbar-expand-md navbar-light">
                    <a class="navbar-brand" href="index.html">
{{--                        <img src="assets/img/logo.png" alt="Logo">--}}
                        <img src="assets/img/logo_2.png" alt="Logo">

                    </a>
                    <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">

                        @include("partials.nav_items")


                        @include("partials.internet_banking")
                    </div>
                </nav>
            </div>
        </div>
    </div>


    <div class="page-title-area">
        <div class="d-table">
            <div class="d-table-cell">
                <div class="container">
                    <div class="title-content">
                        <h2>Prestige Banking</h2>
                        <ul>
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li>
                                <span>Personal - Prestige Banking</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="service-details-area ptb-100">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="details-item">
                        <div class="details-img">
                            <img src="assets/img/services/service-details1.jpg" alt="Details">
                            <h2>Prestige Banking</h2>
                            <p>The Prestige banking experience was created for
                                those who believe in uncompromising excellence.
                                You are unique and so are your needs. That is why we focus on building solutions to suit your needs as an exceptional individual.
                                Enjoy a wide range of exclusive products and services that are further enhanced as
                                we work with you to build a strong beneficial relationship.

                            </p>
{{--                            <p>--}}
{{--                                HolvBank Prestige Banking is about relationships. Not just the relationship between clients and the Bank,--}}
{{--                                but also the relationship between our clients and our partners.--}}
{{--                                As a Prestige client, you can get to enjoy special offers from selected--}}
{{--                                lifestyle partners, whenever you use your Platinum debit card for payments,--}}
{{--                                giving you value for money and preferential treatment in selected places.--}}


{{--                            </p>--}}
                        </div>
                        <div class="details-business">
                            <div class="row">
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details2.jpg" alt="Details">
                                    </div>
                                </div>
                                <div class="col-sm-6 col-lg-6">
                                    <div class="img">
                                        <img src="assets/img/services/service-details3.jpg" alt="Details">
                                    </div>
                                </div>
                            </div>
                            <h3>Why Prestige Banking</h3>
                            <p>HolvBank Prestige Banking is about relationships. Not just the relationship between clients and the Bank,
                                but also the relationship between our clients and our partners. As a Prestige client, you can get to enjoy
                                special offers from selected lifestyle partners, whenever you use your Platinum debit card for payments,
                                giving you value for money and preferential treatment in selected places.</p>
{{--                            <ul>--}}
{{--                                <li>Complete Guide To Mechanical</li>--}}
{{--                                <li>Business & Consulting Agency</li>--}}
{{--                                <li>Award Wining Business</li>--}}
{{--                            </ul>--}}
{{--                            <p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin. Mliterature from 45 BC, making it over 2000 years old. Lorealintock The Extremes of Good and dummy Eviyr. The standard chunk of Lorem Ipsum used sfertyui ince the 1500s is reproduced below for those interested. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium dom, totam rem ape Donec ornare unde omnis iste natus.</p>--}}
                        </div>
                        <div class="details-faq">
                            <div class="section-title">
                                <span class="sub-title">FAQS'</span>
                                <h2>Frequently Asked Questions</h2>
                                <p>Questions frequently asked by our cherished customers and the
                                    responses to them carefully outlined to solve your issues.
                                </p>
                            </div>
                            <div class="faq-item">
                                <ul class="accordion">
                                    <li>
                                        <a>
                                            Who Qualifies?
                                            <i class='bx bx-plus'></i>
                                            <i class="bx bx-minus two"></i>
                                        </a>
                                        <p>
                                            Any individual earning a minimum salary of $10,000 (salaried) and a minimum
                                            AUM of $100,000 + (non-salaried).
                                        </p>
                                    </li>
                                    <li>
                                        <a>
                                            How we help you and your business
                                            <i class='bx bx-plus'></i>
                                            <i class="bx bx-minus two"></i>
                                        </a>
                                        <p>
                                            Enjoy a wide range of exclusive products and services that are further
                                            enhanced as we work with you to build a strong beneficial relationship.
                                        </p>
                                    </li>
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What is Firewall and why it is used?--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
{{--                                    <li>--}}
{{--                                        <a>--}}
{{--                                            What steps will you take to remove risk of finance--}}
{{--                                            <i class='bx bx-plus'></i>--}}
{{--                                            <i class="bx bx-minus two"></i>--}}
{{--                                        </a>--}}
{{--                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua ultrices grad </p>--}}
{{--                                    </li>--}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="widget-area">
                        <div class="services widget-item">
                            <h3>Services List</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        Cash Investment
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Personal Insurance
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Education Loan
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        Financial Planning
                                        <i class='bx bx-right-arrow-alt'></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="download widget-item">
                            <h3>Download</h3>
                            <ul>
                                <li>
                                    <a href="#">
                                        <i class='bx bxs-file-pdf'></i>
                                        Presentation pdf
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <i class='bx bx-notepad'></i>
                                        Wordfile.doc
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="contact widget-item">
                            <h3>Contact</h3>
                            <form>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your email">
                                </div>
                                <div class="form-group">
                                    <textarea id="your-message" rows="8" class="form-control" placeholder="Message"></textarea>
                                </div>
                                <button type="submit" class="btn common-btn">
                                    Send Message
                                    <span></span>
                                </button>
                            </form>
                        </div>
                        <div class="consultation">
                            <img src="assets/img/services/service-details4.jpg" alt="Details">
                            <div class="inner">
                                <h3>Need Any Consultation</h3>
                                <a class="common-btn" href="{{route("contact-us")}}">
                                    Send Message
                                    <span></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
